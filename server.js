var express = require('express'),
    http    = require('http'),
    mongo   = require('mongoskin');

var db = mongo.db('localhost:27017/comments', { safe: true });

var app = express()
  .use(express.static(__dirname, '/'))
  .use(express.json())
  .use(express.logger())
  .use(express.bodyParser());

app.param('commentId', function(req, res, next, commentId) {
  req.commentId = db.collection('comments').id(commentId);
  return next();
});

app.get('/comments/:commentId', function(req, res) {
  db.collection('comments').findOne({ _id: req.commentId }, function(e, result) {
    if (e) return next(e);
    res.send(result);
  });
});

app.get('/comments', function(req, res) {
  db.collection('comments').find({}).toArray(function(e, results) {
    if (e) return next(e);
    res.send(results);
  });
});

app.post('/comments', function(req, res) {
  db.collection('comments').insert(req.body, {}, function(e, results) {
    if (e) return next(e);
    res.send(results);
  });
});

app.del('/comments/:commentId', function(req, res) {
  db.collection('comments').remove({ _id: req.commentId }, function(e, result) {
    if (e) return next(e);
    var message = (result === 1) ? 'success' : 'error';
    res.send({msg: message});
  });
});

app.get('/status', function(req, res) {
  res.json('OK');
});


http.createServer(app)
  .listen(8000, function() {
    console.log('Listening on localhost:8000');
  });
