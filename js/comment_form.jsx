/** @jsx React.DOM */

var CommentForm = React.createClass({
  saveComment: function() {
    app.comments.create({
      email: this.refs.email.getDOMNode().value,
      message: this.refs.message.getDOMNode().value,
      name: this.refs.name.getDOMNode().value
    });
  },
  handleSubmitButtonClick: function() {
    this.saveComment();
    this.props.updateAlert(this.refs.name.getDOMNode().value);
    this.setState({
      email: '',
      message: '',
      name: ''
    });
    this.props.onSubmitButtonClick();
  },
  render: function() {
    return(
      <div className="large-5 columns">
        <div className="row">
          <h2>Write Something</h2>
          <form>
            <div className="row">
              <div className="large-12 columns">
                <textarea
                  ref="message"
                  placeholder="Write your message in markdown..."
                />
              </div>
            </div>
            <div className="row">
              <div className="large-6 columns">
                <label>name</label>
                <input
                  ref="name"
                  type="text"
                  placeholder="John Doe"
                />
              </div>
              <div className="large-6 columns">
                <label>email address</label>
                <input
                  ref="email"
                  type="text"
                  placeholder="john.doe@example.com"
                />
              </div>
            </div>
            <SubmitButton
              onSubmitComment={this.handleSubmitButtonClick}
            />
          </form>
        </div>
      </div>
    );
  }
});
