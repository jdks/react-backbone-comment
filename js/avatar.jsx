/** @jsx React.DOM */

var AuthorAvatar = React.createClass({
  render: function() {
    var imageUrl = "http://www.gravatar.com/avatar/" + md5(this.props.email);

    return(
      <div className="large-3 columns">
        <a className="th">
          <img src={imageUrl}></img>
        </a>
      </div>
    );
  }
});
