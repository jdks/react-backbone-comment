/** @jsx React.DOM */

var Comment = React.createClass({
  render: function() {
    return(
      <article className="panel radius row">
        <Message message={this.props.message} />
        <AuthorAvatar email={this.props.email} />
        <AuthorSignature name={this.props.name} />
      </article>
    );
  }
});
