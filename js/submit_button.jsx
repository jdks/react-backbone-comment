/** @jsx React.DOM */

var SubmitButton = React.createClass({
  handleClick: function() {
    this.props.onSubmitComment();
  },
  render: function() {
    return(
      <div className="row">
        <div className="large-12 columns">
          <a className="button radius large" onClick={this.handleClick} >
            Leave your comment
          </a>
        </div>
      </div>
    );
  }
});
