/** @jsx React.DOM */

var AuthorSignature = React.createClass({
  render: function() {
    return(
      <div className="signature">{this.props.name}</div>
    );
  }
});
