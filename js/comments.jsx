/** @jsx React.DOM */

var app = app || {};

var Comments = React.createClass({
  getInitialState: function() {
    return { comments: app.comments };
  },
  render: function() {
    return(
      <div className="large-6 columns">
        <h2>Comments</h2>
        {
          this.state.comments
            .map(function(comment) {
              return(
                <Comment
                  email={comment.get('email')}
                  message={comment.get('message')}
                  name={comment.get('name')}
                />
              );
            })
            .reverse()
        }
      </div>
    );
  }
});
