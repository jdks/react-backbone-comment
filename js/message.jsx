/** @jsx React.DOM */

var Message = React.createClass({
  converter: new Showdown.converter(),
  render: function() {
    var markup = this.converter.makeHtml(this.props.message);
    return(
      <div
        className="large-9 columns"
        dangerouslySetInnerHTML={{__html: markup}}
      >
      </div>
    );
  }
});
