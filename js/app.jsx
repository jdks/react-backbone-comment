/** @jsx React.DOM */

var app = app || {};

var App = React.createClass({
  getInitialState: function() {
    return {
      alertMsg: 'Waiting for another comment..'
    };
  },
  updateAlert: function(name) {
    this.setState({
      alertMsg: name + ' just left a comment..'
    });
  },
  render: function() {
    return(
      <div>
        <Alert message={this.state.alertMsg} />
        <div id="content" className="row">
          <Comments />
          <CommentForm updateAlert={this.updateAlert} />
        </div>
      </div>
    );
  }
});

React.renderComponent(
  <App />,
  document.getElementById("app")
);
