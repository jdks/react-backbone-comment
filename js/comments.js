var app = app || {};

(function() {
  'use strict';

  var Comment = Backbone.Model.extend({
  });

  var Comments = Backbone.Collection.extend({
    model: Comment,
    url: '/comments'
  });

  app.comments = new Comments();
  app.comments.fetch();
}());
