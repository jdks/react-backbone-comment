/** @jsx React.DOM */

var Alert = React.createClass({
  render: function() {
    return(
      <div data-alert className="alert-box">
        {this.props.message} 
        <a href="#" className="close">&times;</a>
      </div>
    );
  }
});
